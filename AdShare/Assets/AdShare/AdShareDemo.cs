﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StupidWizard.AdShare
{
    public class AdShareDemo : MonoBehaviour
    {
        void Start()
        {
            AdShareManager.Instance.adBannerBot.AddClickAdsListener(OnClickBanner);
        }

        void OnDestroy()
        {
            AdShareManager.Instance.adBannerBot.RemoveClickAdsListener(OnClickBanner);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.B))
            {
                AdShareManager.Instance.LoadBanner();
            }
            if (Input.GetKeyDown(KeyCode.H))
            {
                AdShareManager.Instance.HideBanner();
                AdShareManager.Instance.HideInterstitial();
            }
            if (Input.GetKeyDown(KeyCode.I))
            {
                AdShareManager.Instance.LoadInterstitial();
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                Debug.LogError("Show interstitial = " + AdShareManager.Instance.ShowInterstitial());
            }
        }


        void OnClickBanner(string adId)
        {
            Debug.LogError("OnClick: adId = " + adId);
        }
    }

}

