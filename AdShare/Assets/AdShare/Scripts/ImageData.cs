﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace StupidWizard.AdShare
{
    [Serializable]
    public class ImageData
    {
        private const string SAVED_TIME_UPLOAD_FORM = "AdShare_ImageData_Time";
        private const string SAVED_IMAGE_PATH_FORM = "AdShare_ImageData_Path";

        public string imageId;
        public string url;      // url download
        public int timeUpload;  // yymmddhh


        public string GetImageUrl()
        {
            try
            {
                string path = GetCachedPath();
                if (!string.IsNullOrEmpty(path))
                {
                    return "file://" + path;
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("load cached path error: imageId_" + imageId + " , msg = " + ex.Message);
            }


            return url;
        }

        public void CacheImage(byte[] bytes)
        {
            string tailPath = cachedTailPath;
            SaveBytesData(bytes, tailPath);
        }

        public void ClearImage()
        {
            string path = GetCachedPath();
            if (!string.IsNullOrEmpty(path))
            {
                System.IO.File.Delete(path);
            }
        }

        public bool IsExistCachedFile()
        {
            return (!string.IsNullOrEmpty(GetCachedPath()));
        }


        void SaveBytesData(byte[] bytes, string tailPath)
        {
            string path = System.IO.Path.Combine(Application.persistentDataPath, tailPath);
            System.IO.File.WriteAllBytes(path, bytes);
            SaveTime();
        }



        string GetCachedPath()
        {
            bool cached = IsCached();

            if (cached)
            {
                string tailPath = cachedTailPath;
                if (!string.IsNullOrEmpty(tailPath))
                {
                    string fullPath = System.IO.Path.Combine(Application.persistentDataPath, tailPath);
                    if (System.IO.File.Exists(fullPath))
                    {
                        return fullPath;
                    }
                }
            }

            return string.Empty;
        }


        bool IsCached()
        {
            int savedImageTime = PlayerPrefs.GetInt(savedTimeKey);
            return savedImageTime >= timeUpload;
        }

        void SaveTime()
        {
            PlayerPrefs.SetInt(savedTimeKey, timeUpload);
            PlayerPrefs.Save();
        }

        string savedTimeKey
        {
            get
            {
                return string.Format("{0}_{1}", SAVED_TIME_UPLOAD_FORM, imageId);
            }
        }

        string cachedTailPath
        {
            get
            {
                return string.Format("{0}_{1}", SAVED_IMAGE_PATH_FORM, imageId);
            }
        }
    }

}

