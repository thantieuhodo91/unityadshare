﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LitJson;
using System;

namespace StupidWizard.AdShare
{
    public class AdShareLoader : MonoBehaviour
    {
        private static float TIME_HOUR_TO_UPDATE_JSON = 2;

        private static string TIME_HOUR_LOADED_KEY = "AdShareLoader_TIME_HOUR_LOADED_KEY";
        private static string SAVED_DATA_KEY = "AdShareLoader_SAVED_DATA_KEY";

        [Tooltip("json data file which contains a lot of master dataLink")]
        [SerializeField] TextAsset masterDatalinks;

        [Tooltip("json data file on project")]
        [SerializeField] TextAsset localData;

        public AdShareLink links;
        public AdShareData data;

        void Start()
        {
            LoadData();
        }

        void LoadData()
        {
            links = JsonMapper.ToObject<AdShareLink>(masterDatalinks.text);

            data = JsonMapper.ToObject<AdShareData>(localData.text);

            LoadSavedData();

            if (LastTimeDownloadedData() + TIME_HOUR_TO_UPDATE_JSON < TimeYYMMDDHH())
            {
                StartCoroutine(LoadOnlineData());
            }
        }

        void LoadSavedData()
        {
            string savedJsonData = PlayerPrefs.GetString(SAVED_DATA_KEY, "");
            LoadJsonData(savedJsonData);
        }




        IEnumerator LoadOnlineData()
        {
            string url = links.masterDataLink;
            WWW www = new WWW(url);
            yield return www;

            if (string.IsNullOrEmpty(www.error))
            {
                string jsonData = www.text;
                bool isLoadSuccess = LoadJsonData(jsonData);
                if (isLoadSuccess)
                {
                    SaveDownloadedJsonData(jsonData);
                }
            }
            else
            {
                Debug.LogError("Download error: " + www.error);
            }
        }









        /// <summary>
        /// Loads the json data.
        /// </summary>
        /// <returns><c>true</c>, if json data was loaded, <c>false</c> otherwise.</returns>
        /// <param name="jsonData">Json data.</param>
        bool LoadJsonData(string jsonData)
        {
            if (!string.IsNullOrEmpty(jsonData))
            {
                try
                {
                    var loadedData = JsonMapper.ToObject<AdShareData>(jsonData);
                    if (loadedData != null)
                    {
                        data = loadedData;
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError("load jsonData error " + ex.Message + " json: " + jsonData);
                }
            }
            return false;
        }

        void SaveDownloadedJsonData(string jsonData)
        {
            PlayerPrefs.SetString(SAVED_DATA_KEY, jsonData);
            PlayerPrefs.SetInt(TIME_HOUR_LOADED_KEY, TimeYYMMDDHH());
            PlayerPrefs.Save();
        }


        int TimeYYMMDDHH()
        {
            DateTime time = DateTime.Now;
            return (((time.Year%100) * 100 + time.Month) * 100 + time.Day) * 100 + time.Hour;
        }


        int LastTimeDownloadedData()
        {
            return PlayerPrefs.GetInt(TIME_HOUR_LOADED_KEY, 0);
        }








        #region debug
        [ContextMenu("LogJsonData")]
        void LogJsonData()
        {
            Debug.LogError("JsonData: " + JsonMapper.ToJson(data));
            Debug.LogError("JsonLinksData: " + JsonMapper.ToJson(links));
        }
        #endregion
    }

}

