﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace StupidWizard.AdShare
{
    public class ImageLoader : MonoBehaviour
    {
        public void LoadImage(ImageData imageData, Action<Texture2D> loadSuccess, Action loadFail)
        {
            StartCoroutine(ILoadImage(imageData, loadSuccess, loadFail));
        }

        IEnumerator ILoadImage(ImageData imageData, Action<Texture2D> loadSuccess, Action loadFail)
        {
            WWW www = new WWW(imageData.GetImageUrl());
            yield return www;

            // check if local download error -> download from host
            if (!string.IsNullOrEmpty(www.error))
            {
                imageData.ClearImage();
                yield return null;

                www = new WWW(imageData.GetImageUrl());
                yield return www;
            }

            // check if downloaded data on local
            if (string.IsNullOrEmpty(www.error))
            {
                // have data (maybe con local)
                Texture2D texture = www.texture;
                if ((texture.width <= 0) || (texture.height <= 0) ||
                    ((texture.width == 8) && (texture.height == 8)))
                {
                    // but image file is invalid or corrupted
                    imageData.ClearImage();
                    yield return null;
                    www = new WWW(imageData.GetImageUrl());
                    yield return www;
                }
            }

            // available image of local or image from host
            if (string.IsNullOrEmpty(www.error))
            {
                if (!imageData.IsExistCachedFile())
                {
                    imageData.CacheImage(www.bytes);
                }
                if (loadSuccess != null)
                {
                    loadSuccess(www.texture);
                }
            }
            else
            {
                if (loadFail != null)
                {
                    loadFail();
                }
            }
        }
    }

}
