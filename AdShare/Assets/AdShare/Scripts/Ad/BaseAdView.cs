﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace StupidWizard.AdShare
{
    [RequireComponent(typeof(ImageLoader))]
    [RequireComponent(typeof(RawImage))]
    [RequireComponent(typeof(Button))]
    public class BaseAdView : MonoBehaviour
    {
        protected Action<string> OnAdClickListener;
        protected Action<string> OnLoadedListener;
        protected Action<string> OnLoadFailedListener;

        [SerializeField] protected RawImage _image;
        [SerializeField] protected ImageLoader _loader;

        [SerializeField] protected int countRetryMax = 5;

        protected ImageData imageData;
        protected string gameId;
        protected int countRetry = 0;

        protected virtual void Start()
        {
            GetComponent<Button>().onClick.AddListener(OnAdClick);
        }


        public virtual void LoadAd(string gameId, ImageData imageData)
        {
            this.gameId = gameId;
            this.imageData = imageData;
            countRetry = 0;
            loader.LoadImage(imageData, OnLoadSuccess, OnLoadFailed);
            image.enabled = false;
        }


        #region event
        protected virtual void OnLoadSuccess(Texture2D texture)
        {
            image.texture = texture;
            float height = image.rectTransform.sizeDelta.y;
            float width = (height * texture.width) / texture.height;
            image.rectTransform.sizeDelta = new Vector2(width, height);
            image.enabled = true;

            if (OnLoadedListener != null)
            {
                OnLoadedListener(imageData.imageId);
            }
        }

        protected virtual void OnLoadFailed()
        {
            countRetry++;
            if (countRetry < countRetryMax)
            {
                loader.LoadImage(imageData, OnLoadSuccess, OnLoadFailed);
            }
            else
            {
                if (OnLoadFailedListener != null)
                {
                    OnLoadFailedListener(imageData.imageId);
                }
            }
        }



        protected virtual void OnAdClick()
        {
            Application.OpenURL("http://play.google.com/store/apps/details?id=" + gameId);
            if (OnAdClickListener != null)
            {
                OnAdClickListener(imageData.imageId);
            }
            gameObject.SetActive(false);
        }
        #endregion






        #region add-remove listener

        public virtual void AddLoadedListener(Action<string> pListener)
        {
            OnLoadedListener -= pListener;
            OnLoadedListener += pListener;
        }

        public virtual void RemoveLoadedListener(Action<string> pListener)
        {
            OnLoadedListener -= pListener;
        }

        public virtual void AddLoadFailedListener(Action<string> pListener)
        {
            OnLoadFailedListener -= pListener;
            OnLoadFailedListener += pListener;
        }

        public virtual void RemoveLoadFailedListener(Action<string> pListener)
        {
            OnLoadFailedListener -= pListener;
        }

        public virtual void AddClickAdsListener(Action<string> pListener)
        {
            OnAdClickListener -= pListener;
            OnAdClickListener += pListener;
        }

        public virtual void RemoveClickAdsListener(Action<string> pListener)
        {
            OnAdClickListener -= pListener;
        }

        #endregion






        protected RawImage image
        {
            get 
            {
                if (_image == null)
                {
                    _image = GetComponent<RawImage>();
                }
                return _image;
            }
        }

        protected ImageLoader loader
        {
            get
            {
                if (_loader == null)
                {
                    _loader = GetComponent<ImageLoader>();
                }
                return _loader;
            }
        }
    }

}

