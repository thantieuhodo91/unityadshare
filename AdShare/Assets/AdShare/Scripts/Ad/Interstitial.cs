﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace StupidWizard.AdShare
{
    public class Interstitial : BaseAdView
    {
        [Header("Interstitial")]
        [SerializeField] RawImage fog;
        [SerializeField] Button closeBtn;

        bool _isAdAvailable = false;

        protected override void Start()
        {
            base.Start();
            closeBtn.onClick.AddListener(OnCloseClick);
        }

        public override void LoadAd(string gameId, ImageData imageData)
        {
            base.LoadAd(gameId, imageData);
            fog.enabled = false;
            closeBtn.gameObject.SetActive(false);

            image.enabled = false;

            _isAdAvailable = false;
        }

        public void HideAd()
        {
            StopAllCoroutines();
            gameObject.SetActive(false);
        }

        public bool ShowAd()
        {
            fog.enabled = _isAdAvailable;
            closeBtn.gameObject.SetActive(_isAdAvailable);
            image.enabled = _isAdAvailable;
            return _isAdAvailable;
        }

        protected override void OnLoadSuccess(Texture2D texture)
        {
            SetupImage(texture);
            _isAdAvailable = true;

            if (OnLoadedListener != null)
            {
                OnLoadedListener(imageData.imageId);
            }
        }

        void SetupImage(Texture2D texture)
        {
            image.texture = texture;
            var fogRect = fog.rectTransform.rect;
            float width = fogRect.width;
            float height = fogRect.height;
            if (fogRect.width >= fogRect.height)
            {
                if (texture.width >= texture.height)
                {
                    width = fogRect.width;
                    height = (width * texture.height) / texture.width;
                    image.rectTransform.localRotation = Quaternion.Euler(0, 0, 0);
                }
                else
                {
                    height = fogRect.width;
                    width = (height * texture.width) / texture.height;
                    image.rectTransform.localRotation = Quaternion.Euler(0, 0, 90);
                }
            }
            else
            {
                if (texture.width < texture.height)
                {
                    height = fogRect.height;
                    width = (height * texture.width) / texture.height;
                    image.rectTransform.localRotation = Quaternion.Euler(0, 0, 0);
                }
                else
                {
                    width = fogRect.height;
                    height = (width * texture.height) / texture.width;
                    image.rectTransform.localRotation = Quaternion.Euler(0, 0, -90);
                }
            }

            image.rectTransform.sizeDelta = new Vector2(width, height);
        }

        void OnCloseClick()
        {
            fog.enabled = false;
            closeBtn.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }


        public bool isAdAvailable
        {
            get { return _isAdAvailable; }
        }

    }

}

