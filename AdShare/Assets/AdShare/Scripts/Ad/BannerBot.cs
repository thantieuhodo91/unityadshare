﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace StupidWizard.AdShare
{
    [RequireComponent(typeof(ImageLoader))]
    public class BannerBot : BaseAdView
    {
        protected override void OnLoadSuccess(Texture2D texture)
        {
            image.texture = texture;
            float height = image.rectTransform.sizeDelta.y;
            float width = (height * texture.width) / texture.height;
            image.rectTransform.sizeDelta = new Vector2(width, height);
            image.enabled = true;
            if (OnLoadedListener != null)
            {
                OnLoadedListener(imageData.imageId);
            }
        }
    }
}

