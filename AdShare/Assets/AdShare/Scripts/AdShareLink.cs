﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AdShareLink
{
    public string[] listMasterDataLinks;


    public string masterDataLink
    {
        get
        {
            int id = Random.Range(0, listMasterDataLinks.Length);
            return listMasterDataLinks[id];
        }
    }
}
