﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace StupidWizard.AdShare
{
    public class AdShareManager : MonoBehaviour
    {
        public static AdShareManager Instance;

        [SerializeField] AdShareLoader dataLoader;

        [SerializeField] BannerBot bannerBot;
        [SerializeField] Interstitial interstitial;

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                GameObject.DontDestroyOnLoad(gameObject);
            }
            else
            {
                GameObject.DestroyImmediate(gameObject);
            }
        }


        public void LoadBanner()
        {
            var gameDataTarget = randomGameData;
            if (randomGameData != null)
            {
                bannerBot.gameObject.SetActive(true);
                Debug.LogError("LoadAd Banner: GameData: " + LitJson.JsonMapper.ToJson(gameDataTarget.bannerData));
                bannerBot.LoadAd(gameDataTarget.packageName, gameDataTarget.bannerData);
            }
        }

        public void HideBanner()
        {
            bannerBot.gameObject.SetActive(false);
        }

        public void LoadInterstitial()
        {
            var gameDataTarget = randomGameData;
            if (randomGameData != null)
            {
                interstitial.gameObject.SetActive(true);
                Debug.LogError("LoadAd Interstitial: GameData: " + LitJson.JsonMapper.ToJson(gameDataTarget.interstitialData));
                interstitial.LoadAd(gameDataTarget.packageName, gameDataTarget.interstitialData);
            }
        }

        public bool ShowInterstitial()
        {
            interstitial.gameObject.SetActive(true);
            return interstitial.ShowAd();
        }

        public void HideInterstitial()
        {
            interstitial.HideAd();
        }



        GameData randomGameData
        {
            get
            {
                var dataArray = dataLoader.data.gameDatas;
                if (dataArray != null && dataArray.Length > 0)
                {
                    int id = Random.Range(0, dataArray.Length);
                    while (dataArray[id].packageName.CompareTo(Application.identifier) == 0 && dataArray.Length >= 2)
                    {
                        id = Random.Range(0, dataArray.Length);
                    }
                    return dataArray[id];
                }
                return null;
            }
        }



        public BannerBot adBannerBot
        {
            get
            {
                return bannerBot;
            }
        }

        public Interstitial adInterstitial
        {
            get
            {
                return interstitial;
            }
        }
    }

}
