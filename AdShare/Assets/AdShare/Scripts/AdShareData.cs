﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

namespace StupidWizard.AdShare
{
    [Serializable]
    public class GameData
    {
        public string packageName;
        public ImageData bannerData;
        public ImageData interstitialData;
        public ImageData iconData;
    }

    [Serializable]
    public class AdShareData
    {
        public GameData[] gameDatas;
    }

}

